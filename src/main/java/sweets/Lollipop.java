package sweets;

public class Lollipop extends Sweet {

    public Lollipop(double weight, double price, double sugarContent, int energyValue) {
        super(weight, price, sugarContent, energyValue);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
