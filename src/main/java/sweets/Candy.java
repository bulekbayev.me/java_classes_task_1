package sweets;

public class Candy extends Sweet {

    public Candy(double weight, double price, double sugarContent, int energyValue) {
        super(weight, price, sugarContent, energyValue);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
