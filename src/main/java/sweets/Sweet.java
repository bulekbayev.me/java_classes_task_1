package sweets;

import java.util.Objects;

public class Sweet {

    private double weight;
    private double price;
    private double sugarContent;
    private int energyValue;

    public Sweet(double weight, double price, double sugarContent, int energyValue) {
        this.weight = weight;
        this.price = price;
        this.sugarContent = sugarContent;
        this.energyValue = energyValue;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSugarContent() {
        return sugarContent;
    }

    public void setSugarContent(double sugarContent) {
        this.sugarContent = sugarContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sweet sweet = (Sweet) o;
        return Double.compare(sweet.weight, weight) == 0 &&
                Double.compare(sweet.price, price) == 0 &&
                Double.compare(sweet.sugarContent, sugarContent) == 0 &&
                energyValue == sweet.energyValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, price, sugarContent, energyValue);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
