import java.util.*;

public class Present {

    private double weight;
    private double price;
    private Set<Sweet> setOfCandies;

    public Present() {
        this.weight = 0;
        this.price = 0;
        this.setOfCandies = new HashSet<>();
    }

    public double getWeight() {
        return getTotalWeight();
    }

    public double getPrice() {
        return getTotalPrice();
    }

    public void addSweetsToPresent(Sweet sweet) {
        if (setOfCandies.isEmpty()) {
            setOfCandies.add(sweet);
        } else {
            if (isPresentHaveThatTypeOfCandy(sweet)) {
                System.out.println("You already have " + sweet.toString() + " in your present!");
            } else {
                setOfCandies.add(sweet);
            }
        }
    }

    public boolean isPresentHaveThatTypeOfCandy(Sweet sweet) {
        boolean duplicateCandy = false;
        for (Sweet tempSweet : setOfCandies) {
            if (tempSweet.getClass().equals(sweet.getClass())) {
                duplicateCandy = true;
            }
        }
        return duplicateCandy;
    }

    private double getTotalWeight() {
        for (Sweet tempSweet : this.setOfCandies) {
            this.weight += tempSweet.getWeight();
        }
        return this.weight;
    }

    private double getTotalPrice() {
        for (Sweet tempSweet : this.setOfCandies) {
            this.price += tempSweet.getPrice();
        }
        return this.price;
    }

    public void sortBySugarContent() {
        ArrayList<Sweet> tempList = new ArrayList<>();
        tempList.addAll(this.setOfCandies);
        Comparator<Sweet> comparator = Comparator.comparing(obj -> obj.getSugarContent());
        Collections.sort(tempList, comparator);
        Set<Sweet> sortedSet = new LinkedHashSet<>();
        sortedSet.addAll(tempList);
        this.setOfCandies = sortedSet;
    }

    @Override
    public String toString() {
        return "Present contains " +
                "set of sweets: " + setOfCandies +
                '.';
    }

    public void printPresentsContentWithSugarPercentage() {
        System.out.print("Present's content is ");
        for (Sweet tempSweet : this.setOfCandies) {
            System.out.print(tempSweet.toString() + " : " + tempSweet.getSugarContent() + "%; ");
        }
        System.out.print("\n");
    }

    public void printPresentsWeight() {
        System.out.println("Total weight of the present is " + String.format("%.2f", getWeight()) + " gramm.");
    }

    public void printPresentsPrice() {
        System.out.println("Total price of the present is " + String.format("%.2f", getPrice()) + "$.");
    }

    public void findSweetInRangeOfSugarContent(double sugarContentMinimumValue, double sugarContentMaximumValue) {
        for (Sweet tempSweet : setOfCandies) {
            if (tempSweet.getSugarContent() >= sugarContentMinimumValue && tempSweet.getSugarContent() <= sugarContentMaximumValue) {
                System.out.println(tempSweet.toString() + " " + tempSweet.getSugarContent() + "% is in range of sugar content.");
            }
        }
    }
}