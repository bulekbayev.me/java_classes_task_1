import sweets.*;

public class Solution {

    public static void main(String[] args) {
        Present newYearPresent = new Present();
        Sweet lollipop = new Lollipop(10.0, 22.0,12.75, 675);
        Sweet candy = new Candy(10.0, 22.0,12.75, 675);
        Sweet candy1 = new Candy(10.0, 22.0,12.75, 675);
        Sweet chocolate = new Chocolate(12.0, 28.0,12.75, 675);
        Sweet chocolate1 = new Chocolate(17.0, 22.0,12.75, 675);
        newYearPresent.addSweets(lollipop);
        newYearPresent.addSweets(candy);
        newYearPresent.addSweets(candy1);
        newYearPresent.addSweets(chocolate);
        newYearPresent.addSweets(chocolate1);
        System.out.println("Total weight of the present is " + newYearPresent.getWeight());
        System.out.println("Total price of the present is " + newYearPresent.getPrice());
    }
}
